<?php

$MQTT_HOST = getenv('MQTT_HOST');

$client = new Mosquitto\Client('weatherstation_' . uniqid());
$client->connect($MQTT_HOST, 1883);
$client->publish('weatherstation', json_encode($_GET));
$client->disconnect();