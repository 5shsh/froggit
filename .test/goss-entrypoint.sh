#!/bin/sh

cp /test/env /env

export $(xargs < /env)

php -S 0.0.0.0:8086 -t /app /app/wetterstation.php