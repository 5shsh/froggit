#-------------------------------------------------------------------------------
# Base Image Spec
#-------------------------------------------------------------------------------
ARG BASE_IMAGE_NAMESPACE=library
ARG BASE_IMAGE_NAME=alpine
ARG BASE_IMAGE_VERSION=3.8

FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION} as builder

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------
RUN apk add --no-cache php7-dev mosquitto-dev git autoconf g++ make

RUN git clone https://github.com/mgdm/Mosquitto-PHP.git /mosquittophp

WORKDIR /mosquittophp

RUN phpize
RUN ./configure --with-mosquitto=/usr/lib
RUN make

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#
# -------
#
FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION}

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------
COPY --from=builder /mosquittophp/modules/mosquitto.so /usr/lib/php7/modules/mosquitto.so

RUN apk add --no-cache php7 php7-curl php7-json mosquitto-dev
RUN echo "extension=mosquitto.so" > /etc/php7/conf.d/mosquitto.ini

COPY app /app

ENTRYPOINT ["/app/entrypoint.sh"]

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#--------------------------------------------------------------------------------
# Labelling
#--------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION

LABEL   org.label-schema.name="homesmarthome/froggit" \
        org.label-schema.description="endpoint for froggit" \
        org.label-schema.url="homesmarthome.5square.de" \
        org.label-schema.vcs-ref="$VCS_REF" \
        org.label-schema.vcs-url="$VCS_URL" \
        org.label-schema.vendor="fvsqr" \
        org.label-schema.version=$VERSION \
        org.label-schema.build-date=$BUILD_DATE \
        org.label-schema.schema-version="1.0" \
        org.label-schema.docker.cmd="TBD"